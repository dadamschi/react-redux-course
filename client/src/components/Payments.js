import React, { Component } from 'react';
import StripeCheckout from 'react-stripe-checkout';
import { connect } from 'react-redux';
import * as actions from '../actions';
class Payments extends Component {
  render() {
    return (
      <StripeCheckout
        amount={500} //amount in cents
        name="Test Stripe Box"
        description="This is the Stripe box"
        token={token => this.props.handleToken(token)}     //callback for when after we receive the authorization
        stripeKey={process.env.REACT_APP_STRIPE_KEY}
    >

      <button className="btn">
        Add Credits
      </button>
      </StripeCheckout>
    )
  }

}

export default connect(null, actions)(Payments);